pragma solidity ^0.4.24;

contract ScientificArticleRegistration{
    
 
    address investigativeEditor;
    address consultantEditor;
    address editorial;
    address[] listSpecialistReviewer;
    bytes32 constant resultWrite = "The article was succesfuly";
    bytes32 constant resultApprovalConsultant = "Article approved Consultant";
    
    struct Article
    {
    string title;
    string body;
    int32 isbn;
    string status; 
    bool approvalForConsultantEditor;
    }
    
    
    
    mapping (address => bool) public specialistReviewer;
    mapping (address => Article) public articles;
     
     constructor() public {
        
        editorial = msg.sender;
        
        
    }
    
    
    modifier onlyOwner {
        if (msg.sender != editorial)
        revert();
        _;
    }
    
    
    
    modifier onlyInvestigativeEditor {
        if (msg.sender != investigativeEditor)
        revert();
        _;
    }
    
    modifier onlyConsultantEditor {
        if (msg.sender != consultantEditor)
        revert();
        _;
    }
    
        event writeArticleEvent(string _title,string _body);
        event applicationArticleEvent(string _title,string status);
    
    //Function for write Article, the investigative is the onwner for this function
    function writeArticle(address _investigativeEditor,address _consultantEditor,string _title, string _body)  public returns(bytes32) {
    
        investigativeEditor = _investigativeEditor;
        consultantEditor = _consultantEditor;
        
        require(bytes(_title).length != 0);
        
        articles[investigativeEditor].title = _title;
        articles[investigativeEditor].body = _body;
        articles[investigativeEditor].status = "readyForApproval";
        
        emit writeArticleEvent(articles[investigativeEditor].title,articles[investigativeEditor].body);
        
        return resultWrite;
        
    }
    
    
    //The consultant Editor consult the article wrote for the Ivestigative Editor
    function reviewAprovalArticle(address _investigativeEditor) onlyConsultantEditor public returns (bytes32){
    require(keccak256(bytes(articles[_investigativeEditor].status)) == keccak256("readyForApproval"),"The Article must be readyForApproval");

        investigativeEditor = _investigativeEditor;
        articles[investigativeEditor].approvalForConsultantEditor = true;
        
        return resultApprovalConsultant;
    }
    
    
    //this functon register the Article with the previously approval 
    
    function applicationRegistration() onlyInvestigativeEditor public  payable returns (string){
        require(msg.value >= 2 ether);
        require(articles[msg.sender].approvalForConsultantEditor,"The article need ConsultanEditor Approval");
        
        msg.sender.transfer(msg.value - 2 ether);
        editorial.transfer(2 ether);
        articles[msg.sender].status = "sended";
        
        emit applicationArticleEvent(articles[msg.sender].title,articles[msg.sender].status);
        
        return  articles[msg.sender].status;
    }
    
    //The Editorial review if the proposal of article is into interes area for this magazine.
    function firstApprovalEditorial(address _investigativeEditor) onlyOwner public returns (string) {
        
    require(keccak256(bytes(articles[_investigativeEditor].status)) == keccak256("sended"),"The Article must be sended");

        articles[_investigativeEditor].status = "firstApproved";
        
        return articles[_investigativeEditor].status;
    }
    
    
    //The specialistReviewers approval the articles, first review the previos status.
    function approvalSpecialistReviewer (address _investigativeEditor)  public {

        require(keccak256(bytes(articles[_investigativeEditor].status)) == keccak256("firstApproved"),"The Article must be firstApproved");

        for(uint32 i=0;i<listSpecialistReviewer.length;i++){
            
            if(listSpecialistReviewer[i] == msg.sender){
                
                specialistReviewer[msg.sender] = true;
            }
        }

        
    }
    
    
    //This final approval is create for the Editorial for pay the SpecialistReviewers and change status
    function finalApproval(address _investigativeEditor) onlyOwner public payable{
    
            if(specialistReviewer[listSpecialistReviewer[0]] && specialistReviewer[listSpecialistReviewer[1]]){
            
            articles[_investigativeEditor].isbn++;
            articles[_investigativeEditor].status = "Approved";
            
            msg.sender.transfer(msg.value - 1 ether); 
            listSpecialistReviewer[0].transfer(500 finney);
            listSpecialistReviewer[1].transfer(500 finney);
            
            articles[_investigativeEditor].isbn++;
            articles[_investigativeEditor].status = "Approved";
                
            }
            else{
                revert("this article is not Approved for all specialistReviewer");
            }
        }
    
    
    
    //Add new SpecialistReviewer for validate the article
    function addSpecialistReviewer (address _specialistReviewer) public returns (bool){
        
        require(_specialistReviewer != address(0));
        listSpecialistReviewer.push(_specialistReviewer);
        
        return true;
        
    }
    
}
